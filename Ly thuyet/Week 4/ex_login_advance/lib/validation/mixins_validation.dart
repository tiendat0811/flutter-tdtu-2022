mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!(value!.contains('@') & value.contains('.'))) {
      return 'Pls input valid email.';
    }
    return null;
  }

  String? validatePassword(String? value) {
    RegExp regExp = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[~!@#$%&])\w+');
    if(regExp.hasMatch(value!)) {
      return 'Password must be contain 1 Upper case, 1 lowercase, 1 Special Character.';
    }else if(value.length < 8){
      return 'Password must be at least 8 characters.';
    }
    return null;
  }
}