import 'package:flutter/material.dart';
import 'package:ex1_lab4/screens/inbox.dart';
import 'package:ex1_lab4/screens/phonebook.dart';
import 'package:ex1_lab4/screens/discover.dart';

void main(){
  runApp(MaterialApp(
    title: "Zalo",
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}
class HomePageState extends State<HomePage>{
  int currentIndex = 0;
  final screens = [
    Inboxs(),
    PhoneBook(),
    Discover()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: null,
          ),
        ],
        title: Center(
          child: TextFormField(
            keyboardType: TextInputType.text,
            decoration:
            InputDecoration(icon: Icon(Icons.search), labelText: 'Tìm bạn bè, tin nhắn...'),
            onSaved: (value) {
            },
          ),
        ),
      ),
      body: IndexedStack(
      index: currentIndex,
      children: screens,
    ),

    bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        currentIndex: currentIndex,
        onTap: (index){
          setState(() {
            setState(() {
              currentIndex = index;
            });
          });
        },
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.message),
              label:  "Tin nhắn",
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.contact_phone),
              label:  "Danh bạ",
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.apps),
              label:  "Khám phá",
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.timelapse),
              label:  "Nhật ký",
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outline_rounded),
              label:  "Cá Nhân",
              backgroundColor: Colors.blue
          ),
        ],
      ),
    );
  }
}
