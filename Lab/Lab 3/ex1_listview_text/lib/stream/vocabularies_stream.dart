import 'package:flutter/material.dart';

class VocabularyStream{
  Stream<String> getVocabulary() async*{
    final List<String> vocabularies = [
      "Precise, Exact, Unimpeachable, Perfect, Flawless",
      "Forceful, Overconfident, Insistent, Hardline",
      "Incensed, Fuming, Livid",
      "Irksome, Infuriating, Exasperating",
      "Rested",
      "Atrocious, Horrible, Awful",
      "Astonishing, Charming, Magnificent",
      "Huge, Giant, Tremendous, Humongous",
      "Luminous, Dazzling",
      "Swamped",
      "Serene",
      "Prudent, Cautious",
      "Reckless",
      "Enchanting",
      "Inexpensive, Low-Budget, Stingy",
      "Spotless, Tidy",
      "Obvious"
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t%vocabularies.length;
      return vocabularies[index];
    });
  }
}